"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const net_1 = require("net");
const event_kit_1 = require("event-kit");
const packet_1 = require("./packet");
const splitter_1 = require("./splitter");
const queue_1 = require("./utils/queue");
const defaultOptions = {
    packetResponseTimeout: 2000,
    maxPending: 2
};
class Rcon {
    constructor(options = {}) {
        this.options = options;
        this.emitter = new event_kit_1.Emitter();
        this.requestId = 0;
        this.options = { ...defaultOptions, ...options };
        this.sendQueue = new queue_1.PromiseQueue(this.options.maxPending);
    }
    /**
      Create `Rcon` instance and call the `.connect()` function with options

      @returns A promise that will be resolved after the client is authenticated
      with the server.
    */
    static async connect(options) {
        const rcon = new Rcon();
        await rcon.connect(options);
        return rcon;
    }
    onError(callback) {
        return this.emitter.on("error", callback);
    }
    onDidConnect(callback) {
        return this.emitter.on("did-connect", callback);
    }
    onDidAuthenticate(callback) {
        return this.emitter.on("did-authenticate", callback);
    }
    onDidDisconnect(callback) {
        return this.emitter.on("did-disconnect", callback);
    }
    /**
      Connect and authenticate with the server.

      @returns A promise that will be resolved after the client is authenticated
      with the server.
    */
    async connect(options) {
        if (this.authenticated)
            return Promise.resolve();
        const { host, port } = options;
        this.socket = await new Promise((resolve, reject) => {
            const socket = net_1.createConnection({ host, port }, error => {
                if (error)
                    reject(error);
                else
                    resolve(socket);
                socket.removeListener("error", reject);
            });
            socket.on("error", reject);
            this.connecting = true;
        });
        this.emitter.emit("did-connect");
        this.subscribeToSocketEvents();
        this.connecting = false;
        this.sendQueue.resume();
        const id = this.requestId;
        let packet = await this.sendPacket(packet_1.PacketType.Auth, options.password, true);
        if (packet.id != id || packet.id == -1)
            throw new Error("Authentication failed: wrong password");
        this.authenticated = true;
        this.emitter.emit("did-authenticate", null);
    }
    /**
      Close the connection to the server.
    */
    async disconnect() {
        if (!this.socket)
            return;
        // half close the socket
        this.socket.end();
        this.authenticated = false;
        this.sendQueue.pause();
        await new Promise((resolve, reject) => {
            const listener = this.onDidDisconnect(() => {
                resolve();
                listener.dispose();
            });
        });
    }
    /** Alias for `.disconnect()` */
    async end() {
        await this.disconnect();
    }
    /**
      Send a command to the server.

      @param command The command that will be executed on the server.
      @returns A promise that will be resolved with the command's response from the server.
    */
    async send(command) {
        let packet = await this.sendPacket(packet_1.PacketType.Command, command);
        return packet.payload;
    }
    /**
      Send a raw packet to the server and handle the response packet. If there is no
      connection, wait until the server is authenticated.

      We need to queue the packets before they're sent because minecraft can't
      handle 4 or more packets at once.
    */
    async sendPacket(type, payload, isAuth = false) {
        const id = this.requestId++;
        const createPacketResponsePromise = () => new Promise((resolve, reject) => {
            const timeout = setTimeout(() => {
                packetListener.dispose();
                reject(new Error(`Response timeout for packet id ${id}`));
            }, this.options.packetResponseTimeout);
            const packetListener = this.onPacket(packet => {
                if (!isAuth && packet.id == id || isAuth) {
                    clearTimeout(timeout);
                    packetListener.dispose();
                    resolve(packet);
                }
            });
        });
        const addPacketToQueue = () => this.sendQueue.add(() => {
            this.socket.write(packet_1.encodePacket({ id, type, payload }));
            return createPacketResponsePromise();
        });
        if (isAuth || this.authenticated) {
            return addPacketToQueue();
        }
        else {
            // wait for authentication
            await new Promise((resolve, reject) => {
                const listener = this.onDidAuthenticate(() => {
                    resolve();
                    listener.dispose();
                });
            });
            return addPacketToQueue();
        }
    }
    onPacket(callback) {
        return this.emitter.on("packet", (packet) => {
            callback(packet);
        });
    }
    subscribeToSocketEvents() {
        this.socket.on("close", () => {
            this.authenticated = false;
            this.sendQueue.pause();
            this.socket = null;
            this.emitter.emit("did-disconnect", null);
        });
        this.socket.on("error", error => {
            this.emitter.emit("error", error);
        });
        this.socket
            // Split incoming data into packets
            .pipe(splitter_1.createSplitter())
            .on("data", (data) => {
            this.emitter.emit("packet", packet_1.decodePacket(data));
        });
    }
}
exports.Rcon = Rcon;
