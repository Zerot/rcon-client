"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function encodePacket(packet) {
    const payloadSize = Buffer.byteLength(packet.payload, "utf8");
    const packetSize = payloadSize + 10;
    const buffer = Buffer.allocUnsafe(packetSize + 4);
    buffer.writeInt32LE(packetSize, 0);
    buffer.writeInt32LE(packet.id, 4);
    buffer.writeInt32LE(packet.type, 8);
    buffer.write(packet.payload, 12, packetSize + 2, "utf8");
    buffer.fill(0x00, payloadSize + 12);
    return buffer;
}
exports.encodePacket = encodePacket;
function decodePacket(buffer, offset = 0) {
    const length = buffer.readInt32LE(offset);
    const id = buffer.readInt32LE(offset + 4);
    const type = buffer.readInt32LE(offset + 8);
    const payload = buffer.toString("utf8", offset + 12, length + 2);
    return {
        id, type, payload
    };
}
exports.decodePacket = decodePacket;
var PacketType;
(function (PacketType) {
    PacketType[PacketType["Auth"] = 3] = "Auth";
    PacketType[PacketType["AuthResponse"] = 2] = "AuthResponse";
    PacketType[PacketType["Command"] = 2] = "Command";
    PacketType[PacketType["CommandResponse"] = 0] = "CommandResponse";
})(PacketType = exports.PacketType || (exports.PacketType = {}));
