export interface IPacket {
    id: number;
    type: number;
    payload: string;
}
export declare function encodePacket(packet: IPacket): Buffer;
export declare function decodePacket(buffer: Buffer, offset?: number): IPacket;
export declare enum PacketType {
    Auth = 3,
    AuthResponse = 2,
    Command = 2,
    CommandResponse = 0
}
