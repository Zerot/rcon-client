export interface RconOptions {
    /** @default 2000 ms */
    packetResponseTimeout?: number;
    /** @default 1 */
    maxPending?: number;
}
export declare class Rcon {
    private options;
    private emitter;
    private sendQueue;
    private socket;
    private requestId;
    connecting: boolean;
    authenticated: boolean;
    constructor(options?: RconOptions);
    /**
      Create `Rcon` instance and call the `.connect()` function with options

      @returns A promise that will be resolved after the client is authenticated
      with the server.
    */
    static connect(options: {
        host: string;
        port: number;
        password: string;
    }): Promise<Rcon>;
    onError(callback: (error: any) => any): import("event-kit").Disposable;
    onDidConnect(callback: () => any): import("event-kit").Disposable;
    onDidAuthenticate(callback: () => any): import("event-kit").Disposable;
    onDidDisconnect(callback: () => any): import("event-kit").Disposable;
    /**
      Connect and authenticate with the server.

      @returns A promise that will be resolved after the client is authenticated
      with the server.
    */
    connect(options: {
        host: string;
        port: number;
        password: string;
    }): Promise<void>;
    /**
      Close the connection to the server.
    */
    disconnect(): Promise<void>;
    /** Alias for `.disconnect()` */
    end(): Promise<void>;
    /**
      Send a command to the server.

      @param command The command that will be executed on the server.
      @returns A promise that will be resolved with the command's response from the server.
    */
    send(command: string): Promise<string>;
    /**
      Send a raw packet to the server and handle the response packet. If there is no
      connection, wait until the server is authenticated.

      We need to queue the packets before they're sent because minecraft can't
      handle 4 or more packets at once.
    */
    private sendPacket;
    private onPacket;
    private subscribeToSocketEvents;
}
